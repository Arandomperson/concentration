#include"Piece.hpp"
Piece::Piece(Symbol type)
:mType(type)
,mRevealed(false)
{}

bool Piece::equals(Piece &other){
    return (mType == other.mType);
}

bool Piece::reveal(){ //Returns true if piece was revealed by the method. Returns false if method did nothing because piece was already revealed.
    if(mRevealed == true) return false;
    mRevealed=true; 
    return true;
}

void Piece::hide(){
    mRevealed = false;
}


std::string Piece::toString(){
    if(mRevealed == true)
        return symbolToString(mType);
    return ("?");
}

std::string Piece::symbolToString(Symbol a){
    switch(a){
        case MONKEY: return "M";
        case TIGER: return "T";
        case DOG: return "D";
        case SEAL: return "S";
        case CHEETAH: return "C";
        case RHINO: return "R";
        case PUFFIN: return "P";
        case FALCON: return "F";
        default: return "UNKNOWN";
    }
}