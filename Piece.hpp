#pragma once 
#include<string>
#include"Utils.hpp"
class Piece{
public:
    Piece(Symbol type);
    bool equals(Piece &other);
    bool reveal();
    void hide();
    std::string toString();
    std::string symbolToString(Symbol a);
    ~Piece()=default;
private:
    Symbol mType;
    bool mRevealed;

};