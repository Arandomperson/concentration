#pragma once
#include<string>
#include"Piece.hpp"
#include<vector>
#include<utility>
class Board{
public:
    Board(std::pair<int, int> sizes);
    bool isGameOver();
    bool revealPick(int i, int j);
    void hidePick(int i,int j);
    bool comparePicks(std::pair<int,int> a, std::pair<int,int> b);
    std::string toString();
    std::vector<Symbol> getAllSymbols();
    bool isOutOfBounds(std::pair<int, int>);
    virtual ~Board() {  };
private:
    std::vector<std::vector<Piece> > mBoard;
    int mCounter;
    std::pair<int, int> mSizes; 
};