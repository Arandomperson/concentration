#pragma once
#include<utility>
#include<string>
#include"UI.hpp"
class IPlayer{
public:
    virtual std::pair<int,int> getCoord() = 0;
    virtual std::string toString() = 0;
    virtual std::pair<int,int> getSize() = 0;
};  