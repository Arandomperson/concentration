#include<memory>
#include"Arbiter.hpp"
#include"BotPlayer.hpp"
int main()
{   
Arbiter a(std::make_unique<BotPlayer>( ), std::make_unique<FileBasedUI>() );
a.playGame();
return 0;

}
