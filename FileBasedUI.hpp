#pragma once
#include<string>
#include"UI.hpp"
#include<fstream>
class FileBasedUI:public UI{
public:
    FileBasedUI();
    virtual void printString(std::string s) override;
    virtual std::pair<int, int> readCoord() override;
    std::string readName() override;
    std::pair<int, int> readSize() override;
    std::ifstream f;
    std::ofstream o;
    ~FileBasedUI(){ f.close(); o.close();}
};