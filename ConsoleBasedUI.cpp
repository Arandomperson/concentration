#include"ConsoleBasedUI.hpp"
#include<iostream>
ConsoleBasedUI::ConsoleBasedUI(){}

void ConsoleBasedUI::printString(std::string s) {
    std::cout<<s<<'\n';
}
std::pair<int, int> ConsoleBasedUI::readCoord() {
    int a,b;
    std::cout<<"Reading coords: ";
    std::cin>>a>>b;
    return (std::make_pair(a - 1,b - 1) );
}

std::pair<int, int> ConsoleBasedUI::readSize(){
    printString("Please give number of rows and columns: "); 
    int a,b;
    std::cin>>a>>b;
    return (std::make_pair(a, b) );
} 

std:: string ConsoleBasedUI::readName(){
    std::string mName;
    std::cout<<"Reading name: ";
    std::cin>>mName;
    return mName;
}

// De inlocuit toate metodele de printout din arbitru cu printString(mesaj)