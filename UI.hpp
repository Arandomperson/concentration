#pragma once
#include<string>

class UI{
public:
    virtual std::pair<int,int> readSize() = 0;
    virtual std::string readName() = 0;
    void writeGrid(std::string board);
    void wrongPick() {printString("Picks do not match. Hiding last 2 picks");};
    void retry() {printString("Coords already revealed or out of bounds, please try again");};
    void gameOver() {printString("Congratulation, you won the Concentration Game");}; //implem final
    virtual void printString(std::string s) = 0;
    virtual std::pair<int, int> readCoord() =0 ;
};