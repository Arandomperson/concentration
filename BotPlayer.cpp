#include"BotPlayer.hpp"


BotPlayer::BotPlayer()
:mUI()
,mName( mUI.readName() ){
}

std::pair<int, int> BotPlayer::getCoord(){
    return mUI.readCoord();
}

std::pair<int, int> BotPlayer::getSize(){
    return mUI.readSize();
}
std:: string BotPlayer::toString(){
    return mName;
}
