#include"Arbiter.hpp"
#include"IPlayer.hpp"
#include"Board.hpp"
#include"UI.hpp"
//#include <boost/stacktrace.hpp>
Arbiter::Arbiter(std::unique_ptr<IPlayer> player, std::unique_ptr<UI> uiType)
: mPlayer( std::move(player) )
, mBoard( std::make_unique<Board> ( mPlayer->getSize() ) )
, mUI( std::move(uiType))
{
}

void Arbiter::playGame(){
  std::pair <int,int> mFirstPick;
  std::pair <int,int> mSecondPick;
  do{  

  //std::cout << boost::stacktrace::stacktrace();
  mFirstPick = mPlayer->getCoord();
  checkPick(mFirstPick);  
  mPlayer->mUI.printString( mBoard->toString() ); 
  mSecondPick = mPlayer->getCoord();
  checkPick(mSecondPick);
  mUI->printString( mBoard->toString() );
  if(!mBoard->comparePicks (mFirstPick, mSecondPick) )
    {
      mBoard->hidePick(mFirstPick.first , mFirstPick.second);
      mBoard->hidePick(mSecondPick.first , mSecondPick.second);
      mUI->wrongPick();
      mUI->printString( mBoard->toString() );
    }

  }while (!mBoard->isGameOver());
  
  mUI->gameOver();

}

void Arbiter::checkPick(std::pair <int, int> &coords){
  while( mBoard->isOutOfBounds(coords) || !mBoard->revealPick(coords.first , coords.second) ) //if piece was already revealed. to move to method
  {
    mUI->retry();
    coords = mPlayer->getCoord();
  }
}

Arbiter::~Arbiter( ) { } ;