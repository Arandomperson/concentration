#pragma once
#include<string>
#include"UI.hpp"
class ConsoleBasedUI:public UI{
public:
    ConsoleBasedUI();
    virtual void printString(std::string s) override;
    virtual std::pair<int, int> readCoord() override;
    std::string readName() override;
    std::pair<int, int> readSize() override;
    ~ConsoleBasedUI() = default;
};