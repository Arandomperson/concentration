#include "Board.hpp"
#include<random>
#include<chrono>
#include<algorithm>
#include<iterator>
#include"Utils.hpp"
Board::Board(std::pair<int , int> sizes)
:mBoard(std::vector< std::vector<Piece>>() )
,mCounter(0)
,mSizes(sizes)
{
    std::vector<Symbol> shuffledSymbols;
    std::vector<Symbol> uniqueSymbols = getAllSymbols();
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    shuffle(uniqueSymbols.begin(), uniqueSymbols.end(), std::default_random_engine(seed)); 
    for(Symbol s:uniqueSymbols)
    {
        shuffledSymbols.push_back(s);
        shuffledSymbols.push_back(s);
    }
    auto end = shuffledSymbols.begin();
    std::advance( end, sizes.first*sizes.second -1);
    shuffle(shuffledSymbols.begin(), end , std::default_random_engine(seed)); 

    for(int i=0; i<sizes.first * sizes.second; i++)
    {
        if( i % sizes.second == 0) 
            mBoard.push_back(std::vector<Piece>() );
        mBoard[i/sizes.second].push_back( shuffledSymbols[i] ); 
    }


/*initialize n*m board */
}
bool Board::isOutOfBounds(std::pair<int, int> choice){
    if ( choice.first >= mSizes.first || choice.second >= mSizes.second || choice.first < 0 || choice.second < 0)
        return true;
    return false;
}

bool Board::isGameOver(){
    return(mCounter == mBoard.size() * mBoard[0].size() );
}
//new
bool Board::comparePicks(std::pair<int,int> a, std::pair<int,int> b)
{
    return mBoard[a.first][a.second].equals( mBoard[b.first][b.second] ) ;
}


bool Board::revealPick(int i, int j){

    if( mBoard[i][j].reveal() )
    {
        mCounter++;
        return true;
    }
    return false;
}

void Board::hidePick(int i, int j){
    mCounter--;
    mBoard[i][j].hide();
}

std::string Board::toString(){
    std::string outBoard="";
    for(int i=0;i<mBoard.size();i++){
        for(int j=0;j<mBoard[i].size();j++)
            outBoard.append ( mBoard[i][j].toString() );
        outBoard.push_back( '\n');        
    }
    return outBoard;
}


std::vector<Symbol> Board::getAllSymbols(){
    return {MONKEY, TIGER, DOG, SEAL, CHEETAH, RHINO, PUFFIN, FALCON };
}


