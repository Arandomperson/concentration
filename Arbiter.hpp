#pragma once
#include<memory>
class IPlayer;
class Board;
class UI;
class Arbiter{
public:
    Arbiter(std::unique_ptr<IPlayer> player, std::unique_ptr<UI> uiType);
    void playGame();
    ~Arbiter();
private:
    void checkPick(std::pair <int, int > &coords);
    std::unique_ptr<IPlayer> mPlayer;
    std::unique_ptr<Board> mBoard;
    std::unique_ptr<UI> mUI;
};