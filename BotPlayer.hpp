#pragma once  
#include"IPlayer.hpp"
#include"FileBasedUI.hpp"
#include<string>
class BotPlayer : public IPlayer{

public:
    BotPlayer();
    std::pair<int,int> getCoord() override ;
    std::string toString() override;
    std::pair<int,int> getSize() override;

private:
    FileBasedUI mUI;
    std::string mName;
    
};