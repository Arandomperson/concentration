#include"FileBasedUI.hpp"
FileBasedUI::FileBasedUI(){
    f.open("bot.txt");
    o.open("output.txt");
}

void FileBasedUI::printString(std::string s) {
    //std::cout<<s<<'\n';
    o<<s<<'\n';
}
std::pair<int, int> FileBasedUI::readCoord() {
    int a,b;
    //o<<"Reading coords: ";
    f>>a>>b;
    return (std::make_pair(a - 1,b - 1) );
}

std::pair<int, int> FileBasedUI::readSize(){
    printString("Please give number of rows and columns: "); 
    int a,b;
    f>>a>>b;
    return (std::make_pair(a, b) );
} 

std:: string FileBasedUI::readName(){
    std::string mName;
    f>>mName;   
    return mName;
}