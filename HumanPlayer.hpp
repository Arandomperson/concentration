#pragma once  
#include"IPlayer.hpp"
#include"ConsoleBasedUI.hpp"
#include<string>
class HumanPlayer : public IPlayer{

public:
    HumanPlayer();
    std::pair<int,int> getCoord() override ;
    std::string toString() override;
    std::pair<int,int> getSize() override;

private:
    ConsoleBasedUI mUI;
    std::string mName;
    
    

};