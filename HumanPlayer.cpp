#include"HumanPlayer.hpp"


HumanPlayer::HumanPlayer()
:mUI()
,mName( mUI.readName() ){

}

std::pair<int, int> HumanPlayer::getCoord(){
    return mUI.readCoord();
}

std::pair<int, int> HumanPlayer::getSize(){
    return mUI.readSize();
}
std:: string HumanPlayer::toString(){
    return mName;
}
